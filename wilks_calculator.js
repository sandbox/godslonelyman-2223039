/**
 * @file
 * Javascript for the Wilks Formula Calculator.
 */

(function ($) {
  Drupal.behaviors.WilksFormula = {
    attach: function (context, settings) {

      var FnWilksFormula = {

        //Coefficients for men are:
        coefficients_men: {
          a: -216.0475144,
          b: 16.2606339,
          c: -0.002388645,
          d: -0.00113732,
          e: 7.01863E-06,
          f: -1.291E-08
        },
        //Coefficients for women are:
        coefficients_women: {
          a: 594.31747775582,
          b: -27.23842536447,
          c: 0.82112226871,
          d: -0.00930733913,
          e: 0.00004731582,
          f: -0.00000009054
        },
        //This is the Wilk Formula/Calculation resulting in the Wilks Factor
        calWilksFactor: function (coeff, bw) {
          var result = 500 / (coeff.a +
            (coeff.b * bw) +
            (coeff.c * Math.pow(bw, 2)) +
            (coeff.d * Math.pow(bw, 3)) +
            (coeff.e * Math.pow(bw, 4)) +
            (coeff.f * Math.pow(bw, 5)));
          return result.toFixed(4);
        },
        getWilksFactor: function (gender, body_weight) {
          //The constants are different for men and woman

          //men
          if (gender === '1') {
            if (body_weight < 40.0) {
              return 1.3354;
            }
            if (body_weight > 205.9) {
              return 0.5318;
            }
            return FnWilksFormula.calWilksFactor(FnWilksFormula.coefficients_men, body_weight);
          }

          //women
          if (body_weight < 40.0) {
            return 1.4936;
          }
          if (body_weight > 150.9) {
            return 0.7691;
          }
          return FnWilksFormula.calWilksFactor(FnWilksFormula.coefficients_women, body_weight);
        },
        getValues: function (form) {
          //read user settings from the html form (weights, wo[man],kg/lbs)
          var gender = $(form).find('input[name=wf_gender]:checked').val(),
            unit = $(form).find('input[name=wf_unit]:checked').val(),
            total_weight = $(form).find('input[name=wf_total_weight]').val().replace(",", "."),
            body_weight = $(form).find('input[name=wf_body_weight]').val().replace(",", ".");
          /**
           * The calculation is only works for kilograms
           * so if the user enters them in pounds they first need to be converted
           */
          if (unit === 'lbs') {
            body_weight = FnWilksFormula.convert_lbs_to_kg(body_weight);
            total_weight = FnWilksFormula.convert_lbs_to_kg(total_weight);
          }
          return {
            'gender': gender,
            'total_weight': total_weight,
            'body_weight': body_weight
          };
        },
        convert_lbs_to_kg: function (lbs) {
          return Math.round(lbs * 0.45359237).toFixed(1);
        },
        calculate: function (formElement) {
          if (formElement) {
            var values = FnWilksFormula.getValues(formElement.form),
              wilksFactor = FnWilksFormula.getWilksFactor(values.gender, values.body_weight),
              result = wilksFactor * values.total_weight;
            return parseFloat(result.toFixed(6));
          }
          return 0;
        }
      };

      $('input[name=wf_calculate]').click(function (event) {
        var result = FnWilksFormula.calculate(this);
        $(this).closest('form').find('.wf_result').text(Drupal.t('The Wilks Total is') + ': ' + result);
        event.preventDefault();
        return false;
      });
    }
  };
}(jQuery));
