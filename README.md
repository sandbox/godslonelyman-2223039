CONTENTS OF THIS FILE:
-----------------------

  * Description

Description
----------

Provides block with a Wilks calculator.

The Wilks Coefficient or Wilks Formula is a coefficient that can be used to measure the strength of a powerlifter against other powerlifters despite the different weights of the lifters. Robert Wilks is the author of the formula.

- http://en.wikipedia.org/wiki/Wilks_Coefficient
