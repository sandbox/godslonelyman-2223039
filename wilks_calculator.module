<?php
/**
 * @file
 * This module enables you to display a Wilks Coefficient Calculator
 * To find out what a Wilks Coefficient is see:
 * http://en.wikipedia.org/wiki/Wilks_Coefficient
 */

/**
 * Implements hook_block_info().
 */
function wilks_calculator_block_info() {
  $blocks['wilks_calculator'] = array(
    'info' => t('Wilks Calculator'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function wilks_calculator_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'wilks_calculator':
      $block['content'] = drupal_get_form('wilks_calculator_form');
      break;
  }

  return $block;
}

/**
 * This displays a Wilks Calculator form with settings (female/man & kg/lbs).
 *
 * @return array
 *   Returns the form array
 */
function wilks_calculator_form($node, &$form_state) {
  $form = array();

  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'wilks_calculator') . '/wilks_calculator.js',
    'type' => 'file'
  );

  $form['#prefix'] = '<div id="wilks_calculator">';
  $form['#suffix'] = '</div>';

  $form['wf_gender'] = array(
    '#type' => 'radios',
    '#title' => t('Gender'),
    '#default_value' => 1,
    '#options' => array(
      '1' => t('Man'),
      '0' => t('Woman'),
    ),
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
  );

  $form['wf_unit'] = array(
    '#type' => 'radios',
    '#title' => t('Unit'),
    '#default_value' => 'kg',
    '#options' => array(
      'kg' => t('kg'),
      'lbs' => t('lbs'),
    ),
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
  );
  $form['wf_body_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Body weight'),
    '#size' => '10',
    '#maxlength' => '6',
  );

  $form['wf_total_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Total weight'),
    '#size' => '10',
    '#maxlength' => '6',
  );

  $form['wf_calculate'] = array(
    '#type' => 'submit',
    '#name' => 'wf_calculate',
    '#value' => t('Calculate Wilks Total'),
  );

  $form['wf_result'] = array(
    '#markup' => '<div class="wf_result"></div>',
  );

  return $form;
}
